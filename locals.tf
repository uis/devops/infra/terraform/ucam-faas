locals {
  create_vpc_connector = var.vpc_access == null && var.enable_static_egress_ip

  # Determines which VPC connector should be used for the Cloud Run service.
  vpc_access = local.create_vpc_connector ? {
    connector          = google_vpc_access_connector.main[0].id
    egress             = "ALL_TRAFFIC"
    network_interfaces = null
  } : var.vpc_access
}
