# Changelog

## [0.8.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.7.0...0.8.0) (2025-02-24)

### Features

* add variable to set deletion_protection ([fff9703](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/fff9703a04decdfbf4d648a6289b75159dc2c8ae))

## [0.7.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.6.0...0.7.0) (2025-02-20)

### Features

* add README documentation ([0616b45](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/0616b45bbde8ef2c9c66f6377af0bcf3d1149aac)), closes [#1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/issues/1)

## [0.6.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.5.2...0.6.0) (2025-01-24)

### Features

* move versions.tf.json to versions.tf ([7b421fb](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/7b421fb71358cfe1adafbdd22102d576714e4e9b))

## [0.5.2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.5.1...0.5.2) (2025-01-08)

### Bug Fixes

* add grant_sql_client_role_to_cloud_run_sa to fix mounting postgres ([95371f2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/95371f21fca9d1f327580ef1479b2bba9b282933))

## [0.5.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.5.0...0.5.1) (2024-12-20)

### Bug Fixes

* correct dev examples ([b1fcdde](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/b1fcdde435b66acd54239fdb18d7ab3b1e69c5be))
* correct errors in pre-deploy implementation ([b439b1f](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/b439b1f5b17c86a65e905918592d73e8f1acfb5f))
* correct iam binding dependency ([8bcc73e](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/8bcc73efd2f162294e052a39cf277445b4f693dd))

## [0.5.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.4.0...0.5.0) (2024-12-17)

### Features

* add sql instance mount variable ([dc4fc12](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/dc4fc12127cb0d43d226757c43d5ae5fddd3c27d))

## [0.4.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.3.0...0.4.0) (2024-12-11)

### Features

* add pre-deploy job ([fa3aecc](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/fa3aecc78ce4590727e637497b635c88f14d9572))

### Bug Fixes

* update gitignore to include terraform lock ([50fb46f](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/50fb46f90c1b1a08c85c19321bcb6639cd327fc6))

## [0.3.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.2.1...0.3.0) (2024-11-27)

### Features

* add static egress IP capabilities ([9729848](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/97298481a4bd62f68bfc81c4b7953d83e64ae6af))

## [0.2.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.2.0...0.2.1) (2024-11-05)

### Bug Fixes

* timeout from var instead of local ([faf4c3a](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/faf4c3ae6fd57583e7f66199b7a9f6a93e5df281))

## [0.2.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.1.6...0.2.0) (2024-10-21)

### Features

* set CloudRun timeout to timeout_seconds variable ([974ee1e](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/commit/974ee1ef02d65cd47534d4ca2837a1d611b2978f))

## [0.1.6](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.1.5...0.1.6) (2024-08-21)

## [0.1.5](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.1.4...0.1.5) (2024-08-20)

## [0.1.4](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.1.3...0.1.4) (2024-07-24)

## [0.1.3](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/ucam-faas/compare/0.1.2...0.1.3) (2024-05-20)

## [0.1.2]

- Weaken provider version constraints

## [0.1.1]

### Added

- Moved from combined repo: https://gitlab.developers.cam.ac.uk/uis/devops/ucam-faas-python/ucam-faas/-/tree/3754a151179d21f838fc184351e331ea15d99a95/terraform
- No additional features but prepared for terraform registry
