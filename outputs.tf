#output "dead_letter_topic_id" {
#  value       = google_pubsub_topic.dead_letter.id
#  description = "The ID of the dead letter Pub/Sub topic."
#}

output "service" {
  description = "Webapp Cloud Run service resource"
  value       = google_cloud_run_v2_service.faas
}

output "service_account" {
  description = "Service account which service runs as"
  value       = google_service_account.cloud_run_sa
}

output "dead_letter_topic_id" {
  description = "PUB/SUB dead letter topic for trigger/subscription."
  value       = google_pubsub_topic.dead_letter.id
}
