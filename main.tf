# FIXME Currently subscription and cloud service account are shared
resource "google_service_account" "cloud_run_sa" {

  project    = var.project
  account_id = "${var.name}-cloud-run"

}

# Grant the cloud run service account the ability to connect to the SQL
# instance via the grant_sql_client_role_to_cloud_run_sa boolean variable.
resource "google_project_iam_member" "cloud_run_sql_client" {
  count = var.grant_sql_client_role_to_cloud_run_sa ? 1 : 0

  project = var.project
  role    = "roles/cloudsql.client"
  member  = "serviceAccount:${google_service_account.cloud_run_sa.email}"
}

resource "google_cloud_run_service_iam_binding" "binding" {
  location = var.location
  service  = google_cloud_run_v2_service.faas.name
  role     = "roles/run.invoker"
  members  = ["serviceAccount:${google_service_account.cloud_run_sa.email}"]
}

resource "google_project_service_identity" "pubsub_agent" {
  provider = google-beta
  project  = var.project
  service  = "pubsub.googleapis.com"
}

# This only providing access to a role to the service account/pubsub
#trivy:ignore:AVD-GCP-0011
resource "google_project_iam_binding" "project_token_creator" {
  project = var.project
  role    = "roles/iam.serviceAccountTokenCreator"
  members = ["serviceAccount:${google_project_service_identity.pubsub_agent.email}"]
}

resource "google_cloud_run_v2_service" "faas" {
  name     = var.name
  location = var.location

  ingress = "INGRESS_TRAFFIC_INTERNAL_ONLY"

  deletion_protection = var.deletion_protection

  template {

    service_account = google_service_account.cloud_run_sa.email

    containers {
      image = var.function.container_image
      args  = var.function.cmd != null ? [var.function.cmd] : null

      dynamic "env" {
        for_each = var.function.env
        content {
          name  = env.value["name"]
          value = env.value["value"]
          dynamic "value_source" {
            for_each = env.value["value_source"] != null ? [env.value["value_source"]] : []
            content {
              dynamic "secret_key_ref" {
                for_each = value_source.value["secret_key_ref"] != null ? [value_source.value["secret_key_ref"]] : []
                content {
                  secret  = secret_key_ref.value["secret"]
                  version = secret_key_ref.value["version"]
                }
              }
            }
          }
        }
      }

      dynamic "volume_mounts" {
        for_each = var.mount_cloudsql_instance != null ? [1] : []
        iterator = instance
        content {
          name       = "cloudsql"
          mount_path = "/cloudsql"
        }
      }

      resources {
        limits = {
          cpu    = var.function.cpu_limit
          memory = var.function.memory_limit
        }
      }

      liveness_probe {
        http_get {
          path = "/healthy"
        }
      }
      startup_probe {
        http_get {
          path = "/healthy"
        }
      }

    }

    dynamic "volumes" {
      for_each = var.mount_cloudsql_instance != null ? [1] : []
      iterator = instance
      content {
        name = "cloudsql"
        cloud_sql_instance {
          instances = [var.mount_cloudsql_instance]
        }
      }
    }

    scaling {

      min_instance_count = 0
      max_instance_count = var.concurrency.max_concurrent_functions > var.concurrency.max_container_concurrency ? (var.concurrency.max_concurrent_functions - (var.concurrency.max_concurrent_functions % var.concurrency.max_container_concurrency)) / var.concurrency.max_container_concurrency : 1

    }

    timeout = "${var.timeout_seconds}s"

    max_instance_request_concurrency = var.concurrency.max_concurrent_functions > var.concurrency.max_container_concurrency ? var.concurrency.max_container_concurrency : var.concurrency.max_concurrent_functions

    dynamic "vpc_access" {
      for_each = local.vpc_access != null ? [local.vpc_access] : []
      content {
        connector = vpc_access.value["connector"]
        egress    = vpc_access.value["egress"]
        dynamic "network_interfaces" {
          for_each = vpc_access.value["network_interfaces"] != null ? [vpc_access.value["network_interfaces"]] : []
          iterator = network_interface
          content {
            network    = network_interface.value["network"]
            subnetwork = network_interface.value["subnetwork"]
          }
        }
      }
    }
  }
  depends_on = [
    null_resource.pre_deploy_job_trigger
  ]
}

locals {

  cron_schedules_supplied = length(var.triggers.cron_schedules) > 0

  # Default for cron == null, default for supplied pubsub == 14 days.
  dead_letter_message_retention_seconds = var.dead_letter.message_retention_duration_seconds == null ? (local.cron_schedules_supplied ? 0 : 1209600) : var.dead_letter.message_retention_duration_seconds

}

resource "google_pubsub_subscription" "main" {

  # Set count to aid adding non-pubsub based trigger if needed in the future
  count = 1

  name  = var.name
  topic = local.cron_schedules_supplied ? google_pubsub_topic.scheduler_pubsub[0].id : var.triggers.pubsub_topic_id

  push_config {
    push_endpoint = google_cloud_run_v2_service.faas.uri
    oidc_token {
      service_account_email = google_service_account.cloud_run_sa.email
    }
    attributes = {
      x-goog-version = "v1"
    }
  }

  dead_letter_policy {
    dead_letter_topic     = google_pubsub_topic.dead_letter.id
    max_delivery_attempts = var.retry_count + 1
  }

  retry_policy {
    minimum_backoff = "30s"
    maximum_backoff = "600s"

  }

  expiration_policy {
    ttl = "" # Never expire
  }

  ack_deadline_seconds = var.timeout_seconds

}

resource "google_pubsub_topic" "scheduler_pubsub" {

  count = local.cron_schedules_supplied ? 1 : 0

  name    = "${var.name}-cron"
  project = var.project

}

resource "google_cloud_scheduler_job" "cron" {

  for_each = { for index, cron in var.triggers.cron_schedules : index => cron }

  name    = "${var.name}-${each.key}"
  project = var.project

  schedule  = each.value
  time_zone = "Europe/London"

  pubsub_target {
    topic_name = google_pubsub_topic.scheduler_pubsub[0].id
    data       = base64encode(jsonencode({ "cron_schedule" : each.value }))
  }

}

resource "google_pubsub_topic" "dead_letter" {
  name    = "${var.name}-dl"
  project = var.project

  message_retention_duration = local.dead_letter_message_retention_seconds == 0 ? null : "${local.dead_letter_message_retention_seconds}s"

}

resource "google_pubsub_topic_iam_member" "publish_deadletter_topic" {
  topic  = google_pubsub_topic.dead_letter.name
  role   = "roles/pubsub.publisher"
  member = "serviceAccount:${google_project_service_identity.pubsub_agent.email}"
}


resource "google_pubsub_subscription_iam_member" "deadletter_subscribe" {
  subscription = google_pubsub_subscription.main[0].name
  role         = "roles/pubsub.subscriber"
  member       = "serviceAccount:${google_project_service_identity.pubsub_agent.email}"
}

# Configure a Cloud Run Job which will be executed before the deployment of the google_cloud_run_v2_service.faas
# resource. This is primarily useful to run database migrations, however other use cases may exist.
resource "google_cloud_run_v2_job" "pre_deploy" {
  count = var.enable_pre_deploy_job ? 1 : 0

  name                = "${var.name}-pre-deploy"
  location            = var.location
  project             = var.project
  launch_stage        = var.pre_deploy_job_launch_stage
  deletion_protection = var.deletion_protection

  template {
    labels      = var.pre_deploy_job_labels
    annotations = var.pre_deploy_job_annotations
    parallelism = var.pre_deploy_job_parallelism
    task_count  = var.pre_deploy_job_task_count
    template {
      service_account       = google_service_account.cloud_run_sa.email
      timeout               = var.pre_deploy_job_timeout
      execution_environment = var.pre_deploy_job_execution_environment
      encryption_key        = var.pre_deploy_job_encryption_key
      max_retries           = var.pre_deploy_job_max_retries

      containers {
        name        = var.pre_deploy_job_container.name
        image       = var.pre_deploy_job_container.image
        command     = var.pre_deploy_job_container.command
        args        = var.pre_deploy_job_container.args
        working_dir = var.pre_deploy_job_container.working_dir
        dynamic "env" {
          for_each = var.pre_deploy_job_container.env
          content {
            name  = env.value["name"]
            value = env.value["value"]
            dynamic "value_source" {
              for_each = env.value["value_source"] != null ? [env.value["value_source"]] : []
              content {
                dynamic "secret_key_ref" {
                  for_each = value_source.value["secret_key_ref"] != null ? [value_source.value["secret_key_ref"]] : []
                  content {
                    secret  = secret_key_ref.value["secret"]
                    version = secret_key_ref.value["version"]
                  }
                }
              }
            }
          }
        }
        dynamic "resources" {
          for_each = var.pre_deploy_job_container.resources != null ? [var.pre_deploy_job_container.resources] : []
          iterator = resource
          content {
            limits = resource.value["limits"]
          }
        }
        dynamic "ports" {
          for_each = var.pre_deploy_job_container.ports
          iterator = port
          content {
            name           = port.value["name"]
            container_port = port.value["container_port"]
          }
        }
        dynamic "volume_mounts" {
          for_each = var.pre_deploy_job_container.volume_mounts
          iterator = volume_mount
          content {
            name       = volume_mount.value["name"]
            mount_path = volume_mount.value["mount_path"]
          }
        }
        dynamic "volume_mounts" {
          for_each = var.pre_deploy_job_mount_cloudsql_instance != null ? [1] : []
          iterator = instance
          content {
            name       = "cloudsql"
            mount_path = "/cloudsql"
          }
        }
      }
      dynamic "volumes" {
        for_each = var.pre_deploy_job_volumes
        iterator = volume
        content {
          name = volume.value["name"]
          dynamic "secret" {
            for_each = volume.value["secret"] != null ? [volume.value["secret"]] : []
            content {
              secret       = secret.value["secret"]
              default_mode = secret.value["default_mode"]
              dynamic "items" {
                for_each = secret.value["items"]
                iterator = item
                content {
                  path    = item.value["path"]
                  version = item.value["version"]
                  mode    = item.value["mode"]
                }
              }
            }
          }
          dynamic "cloud_sql_instance" {
            for_each = volume.value["cloud_sql_instance"] != null ? [volume.value["cloud_sql_instance"]] : []
            content {
              instances = cloud_sql_instance.value["instances"]
            }
          }
        }
      }
      dynamic "volumes" {
        for_each = var.pre_deploy_job_mount_cloudsql_instance != null ? [1] : []
        iterator = instance
        content {
          name = "cloudsql"
          cloud_sql_instance {
            instances = [var.pre_deploy_job_mount_cloudsql_instance]
          }
        }
      }
      dynamic "vpc_access" {
        for_each = var.pre_deploy_job_vpc_access != null ? [var.pre_deploy_job_vpc_access] : []
        content {
          connector = vpc_access.value["connector"]
          egress    = vpc_access.value["egress"]
          dynamic "network_interfaces" {
            for_each = vpc_access.value["network_interfaces"] != null ? [vpc_access.value["network_interfaces"]] : []
            iterator = network_interface
            content {
              network    = network_interface.value["network"]
              subnetwork = network_interface.value["subnetwork"]
            }
          }
        }
      }
    }
  }
}

data "google_client_config" "current" {}

# Trigger the pre-deploy job using the gcloud CLI whenever the var.pre_deploy_job_container.image value changes.
resource "null_resource" "pre_deploy_job_trigger" {
  count = var.enable_pre_deploy_job && var.pre_deploy_job_trigger ? 1 : 0

  triggers = merge({
    image_name = var.pre_deploy_job_container.image
    }, var.pre_deploy_job_force ? {
    timestamp = timestamp()
  } : {})

  provisioner "local-exec" {
    command = <<EOI
gcloud --project ${var.project} run jobs execute \
  --region ${var.location} --wait ${google_cloud_run_v2_job.pre_deploy[0].name}
EOI

    environment = {
      # This environment variable tells gcloud CLI to authenticate using an access token. We're using the access token
      # configured in the default google provider via the google_client_config data source.
      CLOUDSDK_AUTH_ACCESS_TOKEN = data.google_client_config.current.access_token
    }
  }

  depends_on = [
    google_cloud_run_v2_job.pre_deploy
  ]
}
