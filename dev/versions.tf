# versions.tf specifies minimum versions for providers and terraform.

terraform {
  required_version = "~> 1.7"

  # Specify the required providers, their version restrictions and where to get them.
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.11"
    }
    google = {
      source  = "hashicorp/google"
      version = "~> 5.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 5.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.6"
    }
  }
}
