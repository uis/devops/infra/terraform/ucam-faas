# backend.tf configures the teraform remote state backend.

terraform {
  backend "gcs" {
    # This bucket has been created by the Powers That Be for our use.
    bucket = "ucam-faas-config-09574972"
    prefix = "terraform/ucam-faas"

    # Product-wide terraform-state service account. This value must hard-coded.
    impersonate_service_account = "terraform-state@ucam-faas-meta-5e4d12ab.iam.gserviceaccount.com"
  }
}
