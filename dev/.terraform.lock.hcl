# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "16.11.0"
  constraints = "~> 16.11"
  hashes = [
    "h1:Hw1BJh3S2wedHr5geP6YBzyfeFwvI/KsEsAr7Box3F0=",
    "h1:dyxmqEcBEzd5VS853KsG/oZo700KSIXDV7XSL+p0vGc=",
    "zh:00e6d68c97c739f320407f76537e58218b52cea0128c01a8995820be79859810",
    "zh:14578344dc44043f537c248e04da766b79587f47327e0dfe7dac4e9e272b7c49",
    "zh:26817d48b62907fe1cc16cface8d6035395c9370dfc39e2fbb1ee7a112c10584",
    "zh:28ad3bdedd76cb7e2290a0b1a2288c5042913d46879644a6776a0fe3e109db12",
    "zh:3882e3d81e751074bf0ae2ee2008c058d6b5b797e8d3f7582c041f7657404c2d",
    "zh:402eda34a8759246f084936bdd511073abb79337ce879a5bba46c065912028f3",
    "zh:6686b2a58e973b570204c63f83f9d5bb7f641406177857fe05619c5679ffda05",
    "zh:8be6c674904b5954d51510663cc74e9d03ec7ee500f0e0e234fe85d9d7d7500c",
    "zh:a0813c74a396d14be8332dffad9f597705af1246bb9b582f149d00c86ad8e24a",
    "zh:bc782bf60c3956c61f52e0c90ab9c125e1c49f2472173ca7edf0bf99fadb05ac",
    "zh:cd9b0e82d2f14e69347c9bb2ecc8fec67238b565bd0f5f5bde4020b98af09e93",
    "zh:ef7582fee9d96d52be020512180801c18e5d4589b4e31588c235c191acbd9ddc",
    "zh:f6f4c313cc90994b122bf0af206d225125d0d7818972b74949944038d8602941",
    "zh:f712c56d17de482a6b4a43ce81d25937a8c9e882e1aa6161f413ab250ae84e65",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version     = "5.42.0"
  constraints = "~> 5.0"
  hashes = [
    "h1:Ck6s8A4fDERSS/UYRG+uwrTA28qObbBPTUKGWf0QQ+I=",
    "h1:YA4cdKjubA3duOLIoZR+Ff6ExEqf2I46xWpAg/6JYRI=",
    "h1:doOxBp84/muXlc+0623fyHdBiXkyxRXqZjJ5dKqVYtY=",
    "h1:eZZMPX/mQ8UUv4YU4UuLSsS3FGNSYL/u4K3QEqNn4/o=",
    "h1:lrYnRlbfpX0TwdA9ro8z3gf9E1BhkcckPqS/r+BEQuc=",
    "h1:n4CJOl5a9YoKMQ2CR4cUUJLro7uVU5zXZjc9rLQmFQg=",
    "h1:ndR5etaoEW98dAhBOLZ3CCxvymIxNuB7NNumUTPNOX8=",
    "h1:olC+pKPVZo36f2r3w+eyugavKiYIHEI0g3+RfxjrvKU=",
    "h1:qs/h+36+WqNZKCkfpxuHMXFSm+jHtxg4+ceypQ75Lx4=",
    "h1:wQfF07k6m0hLh8oEIxuYT5rMlfrCql8gKYYbWA63oXw=",
    "h1:y2SI8CQfV6biOdhsPzxEvlKkxOxc2cPdLiIuMF4+ee8=",
    "zh:2bd5a60bf9938b2c79bb70e1a0b5539b1760944decf6d6404ef67658c97463d6",
    "zh:45cafa6c8fa362e23c5b1a68e15364c33dc14ef00fc9542669c6448d15dee358",
    "zh:51cacbcaaf10b61e7bc274d61b1d7550b99d34c381e4b4be3296c63a89a558a1",
    "zh:8759c09cb091ebb5214da6067cb30e225cf132743aaa005adae5cedc693dbf79",
    "zh:887a7bc530802a33bdfeb26ed089d76828629c5d85058dcc9e87e83215191e73",
    "zh:91b627b831e18fbf5ff56d814d554ce7b2635369e131744a00cf13b3e48c9a61",
    "zh:95a4c3963fed5c229df120499a56a6a8b5b6c2c8652adc830a5bc80d0f946c05",
    "zh:a397245123ef37f9ca3877636cf9cad5b0f2c64fe101f3239896d88c5b0d6b3f",
    "zh:b6a8de64480b3eb706cb0c4ebccd6f56d2500c01aa55bc1fd0b710e62a8c51ed",
    "zh:b7d240b9555866958de977669d5ecb05236d11a9f4b598b0bf05f1a4a6fc9b21",
    "zh:cd430d169af8ce64cbfbbdec9ec3f49be10b4d3f564ad21a29dc0b60a69997d1",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/google-beta" {
  version     = "5.42.0"
  constraints = "~> 5.0"
  hashes = [
    "h1:+hnXYt+Zzz5ZgLVD9PCbjLKClYsZCXPMeA4OkBjgLro=",
    "h1:+vvd4o62n3Y0ui+hVeu/2RINRlt3qIjLFM0VSd+X5cI=",
    "h1:2N3n7Fjmk9LID2AISDVfZVHwhobilKuq9xMVhGcxgMI=",
    "h1:DX6WB9+JD1bIKe2csHreDNdXr66K8125ceHNtsyAq2Q=",
    "h1:H+tooloi4YDGWfcDUG7cJddHCT7lRpiWIAP02hpEVno=",
    "h1:OvF9qhM7Dc5PmmdU0yJV7cU0qla+OSuHLk/CVwWnlx4=",
    "h1:P4bByDFBs/cDpqJ5LmXlVue0FWkNmQVt68eX0SokoxI=",
    "h1:XZFM+tGHPekV6VnUNrgzAM5W9VmZ9rVa5VxFjlA8GQo=",
    "h1:ahemHUtc9Nb/H0kv26ds5wrjmQ7eGLUq1/5AhAEL/q8=",
    "h1:cjmeD+tD/bNw/HNO6/rkZMEK8P6SAzQqkxjJeu5AXR8=",
    "h1:k/3XCfwblRkRwuz2WbHWhRQg02E8KQqpcVRpxtPF3h0=",
    "zh:0105d0f1564f31e7c68260f68fba12a1ed5d3bf5dfb04dd19492b3dd464991e5",
    "zh:050c5dcaf7bc58e5dead9f6caef0c6ca8455bc33510513da2c045d6361e3e32d",
    "zh:0d4ab1667e875822b1c23c817af5cb03eccb60c05f882186f792aa9a27bdf520",
    "zh:160fddc8a41e559cf0545474d98a728ce27f872f86c700aa5534cf33ed68957d",
    "zh:32e93a9bae81b2f7dc9433078616989a2b571447e62da06dbc1493115884fc61",
    "zh:47cac86fbb45ca1cec503d866fe5423f52b937ae53e97fc221f9b783dec0ea05",
    "zh:6cbdff0e9eb506227bf4d22f71e67b3477b67e0af6c7815875a34e376bcea709",
    "zh:7b4c333f7e6f2be21bffeb572c568f0053ee88bf3ad2f1c51560fa84436f44b1",
    "zh:7baa1538a2b190542400f999ed7e0a2c30746b1cab80d5009a166dd881045005",
    "zh:bb83406ca22ca0ff4c48d3008e9989363ce13d6a6e37b0263d79ebdc61cec4f4",
    "zh:ee2a974007519504cf0e9e6aa431fbbde774ba31335d8ec43f0d34c152fe8e84",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version     = "3.2.3"
  constraints = "~> 3.0"
  hashes = [
    "h1:obXguGZUWtNAO09f1f9Cb7hsPCOGXuGdN8bn/ohKRBQ=",
    "zh:22d062e5278d872fe7aed834f5577ba0a5afe34a3bdac2b81f828d8d3e6706d2",
    "zh:23dead00493ad863729495dc212fd6c29b8293e707b055ce5ba21ee453ce552d",
    "zh:28299accf21763ca1ca144d8f660688d7c2ad0b105b7202554ca60b02a3856d3",
    "zh:55c9e8a9ac25a7652df8c51a8a9a422bd67d784061b1de2dc9fe6c3cb4e77f2f",
    "zh:756586535d11698a216291c06b9ed8a5cc6a4ec43eee1ee09ecd5c6a9e297ac1",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:9d5eea62fdb587eeb96a8c4d782459f4e6b73baeece4d04b4a40e44faaee9301",
    "zh:a6355f596a3fb8fc85c2fb054ab14e722991533f87f928e7169a486462c74670",
    "zh:b5a65a789cff4ada58a5baffc76cb9767dc26ec6b45c00d2ec8b1b027f6db4ed",
    "zh:db5ab669cf11d0e9f81dc380a6fdfcac437aea3d69109c7aef1a5426639d2d65",
    "zh:de655d251c470197bcbb5ac45d289595295acb8f829f6c781d4a75c8c8b7c7dd",
    "zh:f5c68199f2e6076bce92a12230434782bf768103a427e9bb9abee99b116af7b5",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version     = "3.6.2"
  constraints = "~> 3.6"
  hashes = [
    "h1:5lstwe/L8AZS/CP0lil2nPvmbbjAu8kCaU/ogSGNbxk=",
    "h1:Gd3WitYIzSYo/Suo+PMxpZpIGpRPrwl0JU0+DhxycFM=",
    "h1:J9EOvuE7qCS/S0lqMX6DNqsh/wq2uhwxE2bOpSn0/hc=",
    "h1:R5qdQjKzOU16TziCN1vR3Exr/B+8WGK80glLTT4ZCPk=",
    "h1:UQlmHGddu39vVzG8kruMsde4GHlG+1S7OLqFApbJvtc=",
    "h1:VYBb5/CQ1tPhV92eUsfxSZ4Ta2OCfNggYwE+Qo+yCD0=",
    "h1:VavG5unYCa3SYISMKF9pzc3718M0bhPlcbUZZGl7wuo=",
    "h1:jke+2u84Hrc7szJKevP1BKFn1o3pfxYhYtity2RPCS8=",
    "h1:m/7/S7a6RzGgeRAJJCsDza2kbaNmFpQDDd849RxD2FE=",
    "h1:uOP0uuF8PKF98YlLqgtjdHBELJLI4BMOOHYXQMYhdlI=",
    "h1:wmG0QFjQ2OfyPy6BB7mQ57WtoZZGGV07uAPQeDmIrAE=",
    "zh:0ef01a4f81147b32c1bea3429974d4d104bbc4be2ba3cfa667031a8183ef88ec",
    "zh:1bcd2d8161e89e39886119965ef0f37fcce2da9c1aca34263dd3002ba05fcb53",
    "zh:37c75d15e9514556a5f4ed02e1548aaa95c0ecd6ff9af1119ac905144c70c114",
    "zh:4210550a767226976bc7e57d988b9ce48f4411fa8a60cd74a6b246baf7589dad",
    "zh:562007382520cd4baa7320f35e1370ffe84e46ed4e2071fdc7e4b1a9b1f8ae9b",
    "zh:5efb9da90f665e43f22c2e13e0ce48e86cae2d960aaf1abf721b497f32025916",
    "zh:6f71257a6b1218d02a573fc9bff0657410404fb2ef23bc66ae8cd968f98d5ff6",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:9647e18f221380a85f2f0ab387c68fdafd58af6193a932417299cdcae4710150",
    "zh:bb6297ce412c3c2fa9fec726114e5e0508dd2638cad6a0cb433194930c97a544",
    "zh:f83e925ed73ff8a5ef6e3608ad9225baa5376446349572c2449c0c0b3cf184b7",
    "zh:fbef0781cb64de76b1df1ca11078aecba7800d82fd4a956302734999cfd9a4af",
  ]
}
