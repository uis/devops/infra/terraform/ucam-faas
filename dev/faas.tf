resource "google_pubsub_topic" "faas_test" {
  name    = "faas-test"
  project = local.project
}

module "faas_service" {

  source  = "./.."
  name    = "faas-test-pubsub"
  project = local.project
  function = {
    container_image = "${local.container_images.function_base}:${local.container_images.function_tag}"
    cmd             = "example_cloud_event"
    env = [
      {
        name  = "TEST_VAR"
        value = "WORKING"
      }
    ]
    memory_limit = "1024Mi"
    cpu_limit    = 2
  }

  timeout_seconds = 10

  triggers = {
    pubsub_topic_id = google_pubsub_topic.faas_test.id
  }

  concurrency = {
    max_concurrent_functions = 1
  }

  alerting = {
    monitoring_scoping_project = local.product_meta_project
    notification_channels      = local.notification_channels
  }

}

module "faas_service_set_retry" {

  source  = "./.."
  name    = "faas-test-set-retry"
  project = local.project
  function = {
    container_image = "${local.container_images.function_base}:${local.container_images.function_tag}"
    cmd             = "example_cloud_event"
    env = [
      {
        name  = "TEST_VAR"
        value = "WORKING"
      }
    ]
  }

  timeout_seconds = 20

  triggers = {
    pubsub_topic_id = google_pubsub_topic.faas_test.id
  }

  retry_count = 4

  concurrency = {
    max_concurrent_functions = 1
  }

  alerting = {
    monitoring_scoping_project = local.product_meta_project
    notification_channels      = local.notification_channels
  }
}

module "faas_service_cron" {

  source  = "./.."
  name    = "faas-test-cron"
  project = local.project
  function = {
    container_image = "${local.container_images.function_base}:${local.container_images.function_tag}"
    cmd             = "example_cloud_event"
    env = [
      {
        name  = "TEST_VAR"
        value = "WORKING"
      }
    ]
  }

  timeout_seconds = 20

  triggers = {
    cron_schedules = ["00 0 */7 * *", "0 12 */7 * *", ]
  }

  retry_count = 4

  concurrency = {
    max_concurrent_functions = 1
  }

  alerting = {
    monitoring_scoping_project = local.product_meta_project
    notification_channels      = local.notification_channels
  }

}

module "faas_service_fail" {

  source  = "./.."
  name    = "faas-test-fail"
  project = local.project
  function = {
    container_image = "${local.container_images.function_base}:${local.container_images.function_tag}"
    cmd             = "example_cloud_event"
    env = [
      {
        name  = "TEST_VAR"
        value = "WORKING"
      }
    ]
  }

  timeout_seconds = 10

  triggers = {
    pubsub_topic_id = google_pubsub_topic.faas_test.id
  }

  retry_count = 4


  concurrency = {
    max_concurrent_functions = 1
  }

  dead_letter = {
    message_retention_duration_seconds = "600"
  }

  alerting = {
    monitoring_scoping_project = local.product_meta_project
    notification_channels      = local.notification_channels
  }
}

module "faas_service_pre_deploy" {

  source  = "./.."
  name    = "faas-test-pre-deploy"
  project = local.project
  function = {
    container_image = "${local.container_images.function_base}:${local.container_images.function_tag}"
    cmd             = "example_raw_event_fail"
    env = [
      {
        name  = "TEST_VAR"
        value = "WORKING"
      }
    ]
  }

  timeout_seconds = 10

  triggers = {
    pubsub_topic_id = google_pubsub_topic.faas_test.id
  }

  concurrency = {
    max_concurrent_functions = 1
  }

  alerting = {
    monitoring_scoping_project = local.product_meta_project
    notification_channels      = local.notification_channels
  }

  enable_pre_deploy_job  = true
  pre_deploy_job_trigger = true
  pre_deploy_job_container = {
    image = "us-docker.pkg.dev/cloudrun/container/hello-job:latest"

    command = ["env"]

    resources = {
      limits = {
        cpu    = 1
        memory = "512Mi"
      }
    }
  }
}
