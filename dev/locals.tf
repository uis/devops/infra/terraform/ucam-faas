# locals.tf contain definitions for local variables which are of general utility
# within the configuration.

# These locals are provided by the DevOps Cloud Team.
locals {
  # Bucket and path for GCP-admin provided configuration.
  config_bucket = "ucam-faas-config-09574972"
  config_path   = "config_v1.json"
}

# These locals come from the manual bootstrapping steps.
locals {
  # Base URLs of GitLab instance holding deployment and webapp projects.
  gitlab_base_url = "https://gitlab.developers.cam.ac.uk/"

  # Name of Secret Manager secret which contains the GitLab access token for
  # this deployment.
  gitlab_access_token_secret_name = "gitlab-dev-deploy-access-token"

  # Project which contains the GitLab access token secret.
  gitlab_access_token_secret_project = "gl-proj-factory-prod-01d25675"

  notification_channels = [local.gcp_config.notification_channels.email.wilson_team]

  # Name of secret in meta project containing workspace-specific secrets. See
  # the README for the format of this secret. If blank, no workspace-specific
  # secrets are used.
  workspace_extra_secrets_secret_name = ""
}

# This data source retrieves the webapp project from GitLab based on the project id
# specified when running copier. This is used in the following locals section.
data "gitlab_project" "faas" {
  id = "8520"
}

# These locals define common configuration parameters for the deployment.
locals {
  # Default region for resources.
  region = "europe-west2"

  # Container images used in this deployment. Generally these
  # should be tagged explicitly with the Git commit SHA for the exact version to
  # deploy. They are specified per-workspace with generic "latest from master"
  # fallbacks if not otherwise specified.
  container_images = lookup({
    development = {
      function_base = join("/", [
        local.gcp_config.artifact_registry_docker_repository,
        "ucam-faas",
        "example",
      ])
      function_tag = "latest"
    }
  }, terraform.workspace, {})
}

# These locals are derived from resources, data sources or other locals.
locals {

  # Project id of product-specific meta project.
  product_meta_project = local.gcp_config.product_meta_project

  # Project id for workspace-specific project.
  project = local.workspace_config.project_id

}
