locals {

  dead_letter_queue_published_alert_message = "${google_pubsub_topic.dead_letter.name} - ${terraform.workspace} - Event entered deadletter queue"
  dead_letter_queue_expire_alert_message    = "${google_pubsub_topic.dead_letter.name} - ${terraform.workspace} - Event(s) about to expire"

  three_days_seconds = 259200
}

resource "google_monitoring_alert_policy" "dead_letter_queue_published" {

  count = var.alerting.alerts.failed_after_retries ? 1 : 0

  display_name = local.dead_letter_queue_published_alert_message
  project      = var.alerting.monitoring_scoping_project
  combiner     = "OR"

  enabled = true

  notification_channels = var.alerting.notification_channels

  conditions {
    display_name = local.dead_letter_queue_published_alert_message

    condition_threshold {
      filter = <<-EOI
        resource.type = "pubsub_topic"
        AND resource.labels.topic_id = "${google_pubsub_topic.dead_letter.name}"
        AND metric.type = "pubsub.googleapis.com/topic/send_request_count"
        AND resource.label.project_id="${var.project}"
      EOI

      duration        = "0s"
      comparison      = "COMPARISON_GT"
      threshold_value = "0"
      aggregations {
        alignment_period     = "180s"
        per_series_aligner   = "ALIGN_SUM"
        cross_series_reducer = "REDUCE_SUM"
      }
    }
  }
}

resource "google_monitoring_alert_policy" "dead_letter_queue_expiring" {

  count = var.alerting.alerts.dead_letter_message_expiring != null ? (var.alerting.alerts.dead_letter_message_expiring ? 1 : 0) : (local.dead_letter_message_retention_seconds == null ? 0 : 1)

  display_name = local.dead_letter_queue_expire_alert_message
  project      = var.alerting.monitoring_scoping_project
  combiner     = "OR"

  enabled = true

  notification_channels = var.alerting.notification_channels

  conditions {
    display_name = local.dead_letter_queue_expire_alert_message

    condition_threshold {
      filter = <<-EOI
        resource.type = "pubsub_topic"
        AND resource.labels.topic_id = "${google_pubsub_topic.dead_letter.name}"
        AND metric.type = "pubsub.googleapis.com/topic/oldest_retained_message_age"
        AND resource.label.project_id="${var.project}"
      EOI

      duration        = "0s"
      comparison      = "COMPARISON_GT"
      threshold_value = local.dead_letter_message_retention_seconds > local.three_days_seconds ? (local.dead_letter_message_retention_seconds - local.three_days_seconds) : 0
      aggregations {
        alignment_period     = "60s"
        per_series_aligner   = "ALIGN_MAX"
        cross_series_reducer = "REDUCE_MAX"
      }
    }
  }
}
