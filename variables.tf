variable "name" {
  type        = string
  description = "The name of the Cloud Run service."
}

variable "function" {

  type = object({
    container_image = string
    cmd             = optional(string, null)
    env = optional(list(object({
      name  = string
      value = optional(string)
      value_source = optional(object({
        secret_key_ref = optional(object({
          secret  = string
          version = optional(string, "latest")
        }))
      }))
    })), [])
    cpu_limit    = optional(number, 1) # null doesn't play well with the state
    memory_limit = optional(string, "512Mi")
  })

  description = <<EOI
    container_image - The container image with the function.
    cmd - Override default function to call. Useful when a container has multiple functions.
    env - Pass in environment variables to function container. Uses same for as cloud run but
      should be list.
    cpu_limit - Maximum CPU limit a function may received. Generally this is the upper limit
      and functions may share this within a single container based on Cloud Run scheduling and
      settings in concurrency.
    memory_limit - Maximum memory limit a function may received. Generally this is the upper limit
      and functions may share this within a single container based on Cloud Run scheduling and
      settings in concurrency.
  EOI
}

variable "location" {
  type        = string
  description = "The location where the service will be deployed."
  default     = "europe-west2"
}

variable "project" {
  type        = string
  description = "The ID of the project where the resources will be deployed."
}

variable "concurrency" {

  type = object({
    max_concurrent_functions  = optional(number, 80)
    max_container_concurrency = optional(number, 8)
  })

  default = {
  }

  validation {
    condition     = (var.concurrency.max_concurrent_functions % var.concurrency.max_container_concurrency) == 0 || (var.concurrency.max_concurrent_functions < var.concurrency.max_container_concurrency)
    error_message = "max_concurrent_functions must be an increment of max_container_concurrency (or less than in which case max_container_concurrency is ignored)"
  }

  description = <<EOI
  Options that determine concurrency of the function.

  max_concurrent_functions - default 80 - total concurrent instances of the function across all containers.
  Must be an increment of max_container_concurrency (or less than in which case max_container_concurrency is ignored)

  max_container_concurrency - default 8 - maximum concurrent functions per container.
  EOI

}

variable "retry_count" {

  type        = number
  default     = 4
  description = "Number of attempts to retry a job."

  validation {
    condition     = var.retry_count >= 4
    error_message = "retry_count >= 4"
  }

}

variable "timeout_seconds" {

  type        = number
  description = "Maximum duration of the function before it timeouts and marked as failed."


}

variable "vpc_access" {
  description = <<EOI
Configure VPC access for the Cloud Run service. For more information on these
options see
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_v2_service#nested_vpc_access
EOI
  type = object({
    connector = optional(string)
    egress    = optional(string)
    network_interfaces = optional(object({
      network    = optional(string)
      subnetwork = optional(string)
      tags       = optional(string)
    }))
  })
  default = null
}

variable "triggers" {

  type = object({

    pubsub_topic_id = optional(string, null)
    cron_schedules  = optional(list(string), [])

  })

  description = <<EOI
  pubsub_topic_id - PUB/SUB topic ID to subscribe to and trigger function from.
  cron_schedules - list of crontab style scheduler strings, e.g. ["30 16 * * 7"]
  See http://man7.org/linux/man-pages/man5/crontab.5.html
  EOI

  validation {
    condition     = var.triggers.pubsub_topic_id != null || length(var.triggers.cron_schedules) > 0
    error_message = "either pubsub_topic_id must be set or cron_schedules must be of length > 0"
  }

  validation {
    condition     = var.triggers.pubsub_topic_id == null || length(var.triggers.cron_schedules) < 1
    error_message = "only one of pubsub_topic_id or cron_schedules must be set"
  }
}

variable "alerting" {

  type = object({
    monitoring_scoping_project = string
    notification_channels      = list(string)
    alerts = optional(object({
      failed_after_retries         = optional(bool, true)
      dead_letter_message_expiring = optional(bool)
    }), {})
  })

  description = <<EOI
    monitoring_scoping_project - The ID of a Cloud Monitoring scoping project to create monitoring resources in.
    notification_channels - A list of notification channel IDs to send uptime alerts to. The format for
      the channel IDs should be "projects/[PROJECT_ID]/notificationChannels/[CHANNEL_ID]".

    alerts -
      failed_after_retries - Will alert if any events aren't processed successfully even after retries
      dead_letter_message_expiring - Will alert if one or more messages in dead letter queue will
        expire within 3 days. Disabled if dead letter messages are not persisted (e.g. cron jobs).
  EOI
}

variable "dead_letter" {

  type = object({
    message_retention_duration_seconds = optional(number)

  })
  default = {}

  description = <<EOI

     Whilst pub/sub deadletter queues are always created per subscription, the messages are not
     always retained and sometimes just used to control retries and help alerting.

     When a topic is provided as a trigger the dead letter queue will retain
     messages for 14 days unless message_retention_duration is set to override.

     For cron jobs, the default is to not to retain messages unless message_retention_duration_seconds
     is set. With no retention policy or subscribers events/messages are lost after retries fail.
     See https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/pubsub_topic#message_retention_duration
     for valid values.

  EOI
}

# Static egress variables
variable "enable_static_egress_ip" {
  default     = false
  type        = bool
  description = <<EOI
Whether to assign a static ip for egress from this cloud run instance. If
enabled, the "vpcaccess.googleapis.com" API must also be enabled on the
project.
EOI
}

variable "static_egress_ip_cidr_range" {
  default     = "10.124.0.0/28"
  type        = string
  description = <<EOI
The cidr range used to create a subnet that this cloud run will use if assigned
a static ip
EOI
}

variable "static_egress_ip_subnetwork_id" {
  description = <<EOI
When using an existing VPC Access Connector with the static egress IP
configuration an existing subnetwork must be provided.
EOI
  type        = string
  default     = null
}

variable "min_ports_per_vm" {
  default     = 64
  type        = number
  description = <<EOI
When using Cloud NAT to provide an egress route, Cloud NAT's minimum ports per
VM can be configured to determine how many concurrent connections can be
established to the same destination IP address and port.
EOI
}

variable "vpc_access_connector_max_throughput" {
  type        = number
  default     = 300
  description = <<EOI
Optional. The maximum throughput of the connector in megabytes per second.
Defaults to 300.
EOI
}

variable "vpc_access_connector_min_throughput" {
  type        = number
  default     = 200
  description = <<EOI
Optional. The minimum throughput of the connector in megabytes per second.
Defaults to 200.
EOI
}

variable "mount_cloudsql_instance" {
  description = <<EOI
Mount a single CloudSQL instance into the FaaS service container. This value
should be the Cloud SQL instance connection name, for example
"example-devel-e662dd2b:europe-west2:sql-6e1dd60b".
EOI
  type        = string
  default     = null
}

variable "grant_sql_client_role_to_cloud_run_sa" {
  description = <<EOI
When set to true the roles/cloudsql.client role will be granted to the cloud
run service account at the project level to allow it to connect to Cloud SQL.
EOI
  type        = bool
  default     = false
}

# Cloud Run pre-deploy job variables
variable "enable_pre_deploy_job" {
  description = <<EOI
Configure a Cloud Run Job to be executed *before* the main Cloud Run service
is deployed. This is useful for running database migrations for example.
EOI
  type        = bool
  default     = false
}

variable "pre_deploy_job_trigger" {
  description = <<EOI
When true, the pre-deploy Cloud Run job is executed via a
null_resource-triggered gcloud command whenever Terraform detects that
var.pre_deploy_job_container.image has changed.
EOI
  type        = bool
  default     = true
}

variable "pre_deploy_job_force" {
  description = <<EOI
When true, and only when used in addition to var.pre_deploy_job_trigger, the
pre-deploy Cloud Run job is executed at every terraform apply, regardless of
# the status of var.pre_deploy_job_container.image.
EOI
  type        = bool
  default     = false
}

variable "pre_deploy_job_labels" {
  description = <<EOI
Map of key/value pairs containing labels to assign to the pre-deploy Cloud Run
job.
EOI
  type        = map(string)
  default     = null
}

variable "pre_deploy_job_annotations" {
  description = <<EOI
Map of key/value pairs containing annotations to assign to the pre-deploy Cloud
Run job.
EOI
  type        = map(string)
  default     = null
}

variable "pre_deploy_job_parallelism" {
  description = <<EOI
Specifies the maximum desired number of tasks the execution should run at
given time.
EOI
  type        = number
  default     = null
}

variable "pre_deploy_job_task_count" {
  description = "Specifies the desired number of tasks the execution should run."
  type        = number
  default     = null
}

variable "pre_deploy_job_launch_stage" {
  description = <<EOI
The launch stage for the pre-deploy Cloud Run job. Possible values are UNIMPLEMENTED,
PRELAUNCH, EARLY_ACCESS, ALPHA, BETA, GA, and DEPRECATED.
EOI
  type        = string
  default     = "GA"
}

variable "pre_deploy_job_container" {
  description = <<EOI
Configure the container instance for the pre-deploy job. See
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_v2_job#nested_containers
for more information on these options.
EOI
  type = object({
    name    = optional(string)
    image   = optional(string)
    command = optional(list(string))
    args    = optional(list(string))
    env = optional(list(object({
      name  = string
      value = optional(string)
      value_source = optional(object({
        secret_key_ref = optional(object({
          secret  = string
          version = optional(string, "latest")
        }))
      }))
    })), [])
    resources = optional(object({
      limits = optional(map(string))
    }))
    ports = optional(list(object({
      name           = optional(string)
      container_port = optional(number)
    })), [])
    volume_mounts = optional(list(object({
      name       = string
      mount_path = string
    })), [])
    working_dir = optional(string)
  })
  default = null
}

variable "pre_deploy_job_volumes" {
  description = <<EOI
Configure one or more volumes for the pre-deploy job. See
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_v2_job#nested_volumes
for more information on these options.
EOI
  type = list(object({
    name = string
    secret = optional(object({
      secret       = string
      default_mode = optional(number)
      items = optional(list(object({
        path    = string
        version = optional(string)
        mode    = optional(number)
      })), [])
    }))
    cloud_sql_instance = optional(object({
      instances = optional(list(string))
    }))
  }))
  default = []
}

variable "pre_deploy_job_timeout" {
  description = "Configure a timeout, in seconds, for the pre-deploy job."
  type        = string
  default     = null
}

variable "pre_deploy_job_execution_environment" {
  description = <<EOI
The execution environment to host this task. Possible values are
EXECUTION_ENVIRONMENT_GEN1, and EXECUTION_ENVIRONMENT_GEN2
EOI
  type        = string
  default     = "EXECUTION_ENVIRONMENT_GEN2"
}

variable "pre_deploy_job_encryption_key" {
  description = <<EOI
The ID of a customer managed encryption key (CMEK) to use to encrypt this
container image.
EOI
  type        = string
  default     = null
}

variable "pre_deploy_job_vpc_access" {
  description = <<EOI
Configure VPC access for the pre-deploy job. See
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_v2_job#nested_vpc_access
for more information on these options.
EOI
  type = object({
    connector = optional(string)
    egress    = optional(string)
    network_interfaces = optional(object({
      network    = optional(string)
      subnetwork = optional(string)
      tags       = optional(string)
    }))
  })
  default = null
}

variable "pre_deploy_job_max_retries" {
  description = "Configure the maximum number of retries for the pre-deploy job."
  type        = number
  default     = null
}

variable "pre_deploy_job_mount_cloudsql_instance" {
  description = <<EOI
Mount a CloudSQL instance in the pre-deploy job container. This is a
convenience variable to simplify mounting a Cloud SQL instance. However, if
you require more control over this you should define it directly in
var.pre_deploy_job_container instead.
EOI
  type        = string
  default     = null
}

variable "deletion_protection" {
  default     = false
  type        = bool
  description = <<EOI
Optional. Whether Terraform will be prevented from destroying
the Cloud Run resource. Defaults to false.
EOI
}
